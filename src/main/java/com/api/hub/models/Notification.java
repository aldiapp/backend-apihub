package com.api.hub.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Notification {
    private String message;
    private String channel;
    private String destination;
}

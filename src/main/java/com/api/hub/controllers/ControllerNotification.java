package com.api.hub.controllers;


import com.api.hub.models.Notification;
import com.api.hub.services.ProducerNotification;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ControllerNotification {

    @Autowired
    private ProducerNotification producerNotification;


    @PostMapping("/NotificationIndividual")
    @ApiOperation("Send notification individuals")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Bad request"),
    })
    public void sendNoficationIndividual(@RequestBody Notification notification){
        producerNotification.sendIndividual(notification);
    }
    @PostMapping("/NotificationMassive")
    @ApiOperation("Send notification massive")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Bad request"),
    })
    public void sendNoficationMassive(@RequestBody List<Notification> notification){
        producerNotification.sendMassive(notification);
    }

}

package com.api.hub.services;

import com.api.hub.models.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProducerNotification {

    @Autowired
    private KafkaTemplate<String, Notification> kafkaTemplate;



    private String topicindividual = "individual";
    private String topicmassive = "massive";


    public void sendIndividual(Notification notification){
        kafkaTemplate.send(topicindividual,notification);
    }

    public void sendMassive(List<Notification> notification){
        for(Notification c: notification){
            kafkaTemplate.send(topicmassive, c);
        }
    }
}
